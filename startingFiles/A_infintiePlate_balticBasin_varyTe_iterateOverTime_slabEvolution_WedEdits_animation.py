#! /usr/bin/env python

import gflex
import numpy as np
from matplotlib import pyplot as plt
import imageio

def test_main(westernFrontalNode=0, lastLoadNode = 4500, Te_1 = 10000., Te_2 = 10000., dx=40., n_nodes = 15000, Te_change_node = 6000, Te_changeZoneWidth=0):

    flex = gflex.F1D()

    flex.Quiet = True

    flex.Method = 'FD' # Solution method: * FD (finite difference)
                       #                  * SAS (superposition of analytical solutions)
                       #                  * SAS_NG (ungridded SAS)

    flex.Solver = 'direct' # direct or iterative
    #convergence = 1E-1 # convergence between iterations, if an iterative solution
                         # method is chosen
    #flex.iterative_ConvergenceTolerance = convergence

    flex.g = 9.8 # acceleration due to gravity
    flex.E = 65E9 # Young's Modulus
    flex.nu = 0.25 # Poisson's Ratio
    flex.rho_m = 3300. # MantleDensity
    flex.rho_fill = 1800. # InfiillMaterialDensity

    flex.Te = Te_1 * np.ones(n_nodes) # Elastic thickness -- scalar but may be an array
    flex.Te[Te_change_node:] = Te_2

    if(Te_changeZoneWidth>0):
        Te_changeZoneWidth_nodes = int( Te_changeZoneWidth / dx )
        Te_change_endNode = Te_change_node + Te_changeZoneWidth_nodes
        print(Te_change_node, Te_change_endNode, Te_changeZoneWidth_nodes)
        flex.Te[Te_change_node:Te_change_endNode] = Te_1 + (Te_2-Te_1) * np.arange(Te_change_endNode-Te_change_node)/(Te_changeZoneWidth_nodes)

    #flex.Te[-3:] = 0
    flex.qs = np.zeros(n_nodes); flex.qs[westernFrontalNode:lastLoadNode] += 4*10E6 # surface load stresses
    flex.dx = dx # grid cell size [m]
    #flex.BC_E = '0Displacement0Slope' # west boundary condition
    flex.BC_E = '0Moment0Shear' # west boundary condition
    flex. BC_W = '0Moment0Shear' # east boundary condition

    flex.initialize()
    flex.run()
    flex.finalize()

    # If you want to plot the output
    #flex.plotChoice='combo'
    # An output file for deflections could also be defined here
    # flex.wOutFile =
    flex.output() # Plots and/or saves output, or does nothing, depending on
                  # whether flex.plotChoice and/or flex.wOutFile have been set

    # TO OBTAIN OUTPUT DIRECTLY IN PYTHON, you can assign the internal variable,
    # flex.w, to another variable -- or as an element in a list if you are looping

    # over many runs of gFlex:
    return flex.w, flex._x_local, flex.Te


####################################################
### ANALYSIS

scaleX = 40

n_outputTimesteps = 60      # Number of timesteps to evaluate over
n_nodes = 2*60000 * scaleX          # Total numer of nodes in the model domain
dx = 40. / scaleX                 # Distance between nodes [m]

#################################################################################
## PLOTTING

convergenceRate = 10000 # m/Myr

initial_westernFrontalNode = 60000 * scaleX

Te_change_node = n_nodes * 3 // 4
Te_1 = 10000.
Te_2 = 14000.

Te_changeZoneWidth = 500 # Width of region where Te changes from Te1 to Te2 starting at Te_change_node

frontMigrationIncrement_nodes = 25 * scaleX
frontMigrationIncrement_meters = frontMigrationIncrement_nodes * dx
frontMigrationIncrement_time = frontMigrationIncrement_meters / convergenceRate

initial_frontalNode = Te_change_node - 5000 * scaleX


plotStratigraphy = False ##change to true to plot stratigraphy
nStratLayers = 10
stratigraphyNodeShift = 30
stratigraphyShift_x = stratigraphyNodeShift * dx
stratigraphyShift_GeologicalTime = stratigraphyShift_x /convergenceRate

geologicalTime = np.empty(n_outputTimesteps)
geologicalTime[0] = -frontMigrationIncrement_time


## Find position of forebuge with no elastic thickness contrast
z, x , Te = test_main( initial_westernFrontalNode, initial_frontalNode, Te_1, Te_2, dx, n_nodes, Te_change_node, Te_changeZoneWidth )   # calculate flexural profile

## Code to find initial forebulge
n_FB = np.argmax( z )
x_FB = x[ n_FB ]
x_DF = x[initial_frontalNode]

deltaX_DF_2_FB = x_FB - x_DF

### Code to find initial basin margin
signChange = z[:-1]*z[1:]     ## Create array where a sign change in z has a negative value
n = signChange<0              ## Create a boolean array of the negative values being TRUE
x_tmp= (x[:-1]+x[1:])/2.      ## Subset x to be the same length as n
x_crossing = x_tmp[n]         ## Return all x values for points where z changes sign
x_basinMargin_initial = x_crossing[0]        ## The basin margin is the smallest value of x as this is the trench side
n_basinMargin_initial = np.argmin( (x-x_basinMargin_initial)**2 )        ## Find the indices of this position

x_basinMargin = np.empty( n_outputTimesteps )
n_basinMargin = np.empty_like( x_basinMargin )
x_deformationFront = np.empty_like( x_basinMargin )
x_highestPoint = np.empty_like( x_basinMargin )
z_deformationFront = np.empty_like( x_basinMargin )
z_highestPoint = np.empty_like( x_basinMargin )


# Loop to progressively propagate deformation front out in increments of 100 * 40 = 4km
# So over 20 updates this is 80km in total (probably enough bto capute pridoli)
for n in range(0,n_outputTimesteps):
    print("Iteration: "+str(n))
    geologicalTime[n] += frontMigrationIncrement_time

    frontalNode = n * frontMigrationIncrement_nodes + initial_frontalNode   # Calculate new deformation front node
    westernFrontalNode = initial_westernFrontalNode + n * frontMigrationIncrement_nodes
    z, x , Te = test_main( westernFrontalNode, frontalNode, Te_1, Te_2, dx, n_nodes, Te_change_node, Te_changeZoneWidth  )   # calculate flexural profile

    ## Find basin margin
    signChange = z[:-1]*z[1:]     ## Create array where a sign change in z has a negative value
    m = signChange<0              ## Create a boolean array of the negative values being TRUE
    x_tmp= (x[:-1]+x[1:])/2.      ## Subset x to be the same length as n
    x_crossing = x_tmp[m]         ## Return all x values for points where z changes sign
    x_basinMargin[n] = x_crossing[0]        ## The basin margin is the smallest value of x as this is the trench side
    n_basinMargin[n] = np.argmin( (x-x_basinMargin[n])**2 )        ## Find the indices of this position

    ## Store DF position with time
    x_deformationFront[n] = frontalNode * dx
    z_deformationFront[n] = z[frontalNode]

    ##Calculate and store the position of the highest point with time
    n_FB = np.argmax( z )
    x_highestPoint[n] = x[ n_FB ]
    z_highestPoint[n] = z[ n_FB ]

time_model = np.arange( n_outputTimesteps )

time_geological = time_model * frontMigrationIncrement_nodes * dx / convergenceRate

plt.plot(Te)
plt.show()

outputFile = "infinitePlateTest_"
images=[]

for n in range(0,n_outputTimesteps,10):

    outputFilename = outputFile+str(n)+".png"

    print("Iteration: "+str(n))
    geologicalTime[n] += frontMigrationIncrement_time

    frontalNode = n * frontMigrationIncrement_nodes + initial_frontalNode   # Calculate new deformation front node

    #z, x , Te = test_main( frontalNode, Te_1, Te_2, dx, n_nodes, Te_change_node, Te_changeZoneWidth  )   # calculate flexural profile


    plt.figure(figsize=(20,18))

    plt.subplot(221)
    plt.plot((x_deformationFront - x_basinMargin_initial) / 1000,  time_geological, label="DF")
    plt.plot((x_basinMargin - x_basinMargin_initial) / 1000  ,  time_geological, label = "BM")
    plt.plot((x_highestPoint - x_basinMargin_initial) / 1000  ,  time_geological, label = "FB")
    plt.axvline( (Te_change_node*dx - x_basinMargin_initial) /1000, linestyle='--', color='red', label = "Te contrast")
    plt.axvline( ((Te_change_node*dx - x_basinMargin_initial) + Te_changeZoneWidth) /1000, linestyle='--', color='red') #second line to show Te transition zone
    plt.axhline(y=0, linestyle='--', color='black', linewidth=0.5)
    plt.text(800,0, "a")
    plt.axhline(y=10, linestyle='--', color='black', linewidth=0.5)
    plt.text(800,10, "b")
    plt.axhline(y=12, linestyle='--', color='black', linewidth=0.5)
    plt.text(800,12, "c")
    plt.axhline(y=14, linestyle='--', color='black', linewidth=0.5)
    plt.text(800,14, "d")
    plt.axhline(y=20, linestyle='--', color='black', linewidth=0.5)
    plt.text(800,20, "e")
    plt.axhline(y=40, linestyle='--', color='black', linewidth=0.5)
    plt.text(800,40, "f")
    plt.title("Deformation front, basin margin and forebuldge migration")

    plt.axhline(time_geological[n], color='green')
    plt.legend()
    plt.xlabel("Postion [km]")
    plt.ylabel("Time (myr)")

    plt.subplot(222)
    plt.plot((z_deformationFront) / 1000,  time_geological, label="DF")
    plt.plot((z_highestPoint) / 1000  ,  time_geological, label = "FB")
    plt.title("Deformation front and forebuldge level changes")
    plt.axhline(y=0, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=10, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=12, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=14, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=20, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=40, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(time_geological[n], color='green')

    plt.axvline (x = 0, color='black', linewidth=0.5, label = "datum")
    plt.legend()
    plt.xlabel("Depth [km]")
    plt.ylabel("Time (myr)")

    plt.subplot(223)

    DF = x_deformationFront
    timeOf_TeChange_Crossing_DF = np.argmin( (DF - Te_change_node*dx)**2 )
    timeOf_TeChange_Crossing_FB = np.argmin( (x_highestPoint - Te_change_node*dx)**2 )
    timeOf_TeChange_Crossing_BM = np.argmin( (x_basinMargin - Te_change_node*dx)**2 )

    plt.plot(-(x_deformationFront - x_basinMargin_initial- (x_basinMargin - x_basinMargin_initial)) / 1000,  time_geological, label="DF-BM")
    #plt.axhline(timeOf_TeChange_Crossing_DF, linestyle='--',color='r',label="Time when Te contrast passes DF")
    #plt.axhline(timeOf_TeChange_Crossing_FB, linestyle='--',color='orange', label="Time when Te contrast passes FB")
    #plt.axhline(timeOf_TeChange_Crossing_BM, linestyle='--',color='g', label="Time when Te contrast passes BM")
    plt.axhline(y=0, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=10, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=12, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=14, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=20, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(y=40, linestyle='--', color='black', linewidth=0.5)

    plt.axhline(time_geological[n], color='green')

    plt.xlabel("Width [km]")
    plt.title("Basin width trough time")
    plt.ylabel("Time (myr)")
    plt.legend()


    ####################################
    plt.subplot(224)
    timeStep = n
    time = timeStep * frontMigrationIncrement_nodes * dx / convergenceRate

    ## Calculate position of basement at this time
    deformationFrontNode = timeStep * frontMigrationIncrement_nodes + initial_frontalNode
    z, x , Te = test_main( deformationFrontNode, Te_1, Te_2, dx, n_nodes, Te_change_node , Te_changeZoneWidth )   # calculate flexural profile

    ## General basin dimension
    distanceBetween_deformationFront_to_TeChange = -( deformationFrontNode - Te_change_node ) * dx / 1000
    xOffset = Te_change_node*dx

    x_Text = deformationFrontNode*dx/1000 - xOffset/1000 + 200

    plt.plot((x-xOffset)/1000, z, label="Slab profile")
    plt.axvline(deformationFrontNode*dx/1000-xOffset/1000, label = "Deformation front")
    plt.axvline(Te_change_node * dx /1000-xOffset/1000, linestyle='--', color='r', label="Elastic thickness change")
    plt.axvline( ((Te_change_node * dx /1000-xOffset/1000) + Te_changeZoneWidth/1000), linestyle='--', color='red')
    plt.xlim((deformationFrontNode*dx/1000-xOffset/1000), (deformationFrontNode*dx/1000-xOffset/1000)+300)
    plt.ylim(-1000,100)
    plt.axhline(y=0, color='black', linewidth=0.5, label="Datum")
    plt.xlabel('x [km]')
    plt.ylabel('y [m]')
    plt.title("a. timestep no " +str(timeStep) + " time: "+str(time) + " myr")
    plt.text(x_Text, -200, "Load-Te offset: "+ str(distanceBetween_deformationFront_to_TeChange)+" km")
    plt.text(x_Text, -250, "Te_1 "+str(Te_1/1000)+" km; Te_2 "+str(Te_2/1000)+" km")
    plt.text(x_Text, -300, "Te_1 to Te_2 transition: " +str(Te_changeZoneWidth/1000.) + " km")
    ## Generate and Plot stratigraphy
    if(plotStratigraphy):
        z_array = np.empty( (10, n_nodes) )

        for n in range(0,nStratLayers):
            nStratLayers = 5
            stratigraphyNodeShift = 30
            stratigraphyShift_x = stratigraphyNodeShift * dx
            stratigraphyShift_GeologicalTime = stratigraphyShift_x /convergenceRate

            geologicalTime = np.empty(n_outputTimesteps)
            geologicalTime[0] = -frontMigrationIncrement_time
            frontalNode = n * stratigraphyNodeShift + deformationFrontNode - nStratLayers*stratigraphyNodeShift   # Calculate new deformation front node
            z_strat, x_strat , Te = test_main(  frontalNode, Te_1, Te_2, dx, n_nodes, Te_change_node, Te_changeZoneWidth  )   # calculate flexural profile
            z_array[n, ] = z_strat

        stratigraphy = np.copy( z_array )
        stratigraphy[ stratigraphy > 0 ]  = 0

        for i in range(0, nStratLayers):
            plt.plot((x-xOffset)/1000, z - stratigraphy[i, ], linestyle='-.')
    plt.text(x_Text,-350, "No of stratigraphic layers: "+str(nStratLayers))
    plt.text(x_Text,-400, "Stratigraphic shift: "+str(stratigraphyNodeShift))
    plt.legend()   # Add legend

    plt.savefig(outputFilename)
    plt.close()

    images.append(imageio.imread(outputFilename))


imageio.mimsave('movie_infinitePlate.gif', images)

## TODOs
## 1. ADD SENSIBE TITLES
## 2. Think about scaling verticle time
## 3. Compare different Te contrast scenarios
###    - Q How long do the transient bahaviour last for?
###    - Q Implicastions for deposition / erosion
