from __future__ import print_function

import imageio

def makeAnim(filename_list, pwd, outputFile="movie.gif", duration=0.3):
    with imageio.get_writer(pwd+'/'+outputFile, mode='I', duration=duration) as writer:
        for filename in filename_list:
            print(filename, end='\r')
            image = imageio.imread(pwd+'/'+filename)
            writer.append_data(image)