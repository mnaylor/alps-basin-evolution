import numpy as np
import matplotlib.pyplot as plt


def Left_Load(a_1,a_2,D_1,D_2,V_0, y, X_y, X, contact):
    """this function calculates the flexure due to a force V_0 at y, where y is to the 
    left (negative sign) of a rigidity contrast.
    """
    M = np.zeros( (8,8) )

    #BC1:
    M[0,0] = 1
    M[0,2] = -1
    M[0,4] = -1

    #BC2:
    M[1,2] = np.exp(y / a_1) * np.cos(-y/a_1)
    M[1,3] = np.exp(y / a_1) * np.sin(-y/a_1)
    M[1,4] = np.exp(-y / a_1) * np.cos(-y/a_1)
    M[1,5] = np.exp(-y / a_1) * np.sin(-y/a_1)
    M[1,6] = -1

    #BC3:
    M[2,0] = 1/a_1
    M[2,1] = 1/a_1
    M[2,2] = 1/a_1
    M[2,3] = -1/a_1
    M[2,4] = -1/a_1
    M[2,5] = -1/a_1

    #BC4:
    M[3,2] = -1/a_1*np.exp(y / a_1) *( np.cos(-y/a_1)+ np.sin(-y/a_1) )
    M[3,3] = 1/a_1*np.exp(y / a_1) *( np.cos(-y/a_1)- np.sin(-y/a_1) )
    M[3,4] = 1/a_1*np.exp(-y / a_1) *( np.cos(-y/a_1)- np.sin(-y/a_1) )
    M[3,5] = 1/a_1*np.exp(-y / a_1) *( np.cos(-y/a_1)+ np.sin(-y/a_1) )
    M[3,6] = 1/a_2
    M[3,7] = -1/a_2

    #BC5:
    M[4,1] = D_1*2/(a_1**2)
    M[4,3] = D_1*2/(a_1**2)
    M[4,5] = D_1*-2/(a_1**2)    
    
    #BC6:
    M[5,2] = D_1*2/(a_1**2)*np.exp(y / a_1)*np.sin(-y / a_1)
    M[5,3] = D_1*-2/(a_1**2)*np.exp(y / a_1)*np.cos(-y / a_1)
    M[5,4] = D_1*-2/(a_1**2)*np.exp(-y / a_1)*np.sin(-y / a_1)
    M[5,5] = D_1*2/(a_1**2)*np.exp(-y / a_1)*np.cos(-y / a_1)
    M[5,7] = D_2*2/(a_2**2)
    
    #BC7:
    M[6,2] = D_1*2/(a_1**3)*np.exp(y / a_1)*(np.cos(-y / a_1) - np.sin(-y / a_1))
    M[6,3] = D_1*2/(a_1**3)*np.exp(y / a_1)*(np.cos(-y / a_1) + np.sin(-y / a_1))
    M[6,4] = -D_1*2/(a_1**3)*np.exp(-y / a_1)*(np.cos(-y / a_1) + np.sin(-y / a_1))
    M[6,5] = D_1*2/(a_1**3)*np.exp(-y / a_1)*(np.cos(-y / a_1) - np.sin(-y / a_1))
    M[6,6] = -2*D_2/(a_2**3)
    M[6,7] = -2*D_2/(a_2**3)
    
    #BC8:
    M[7,0] = -2*D_1/(a_1**3)
    M[7,1] = 2*D_1/(a_1**3)
    M[7,2] = -2*D_1/(a_1**3)
    M[7,3] = -2*D_1/(a_1**3)
    M[7,4] = 2*D_1/(a_1**3)
    M[7,5] = -2*D_1/(a_1**3)
    
    #Right hand side of the simultaneous equations, with the 8th element = -V_0:
    m = m = np.zeros(8)
    m[7] = -V_0
    #solve the matrix equation:
    
    a = np.linalg.solve(M,m)
    A1 = a[0]
    A2 = a[1]
    B1 = a[2]
    B2 = a[3]
    B3 = a[4]
    B4 = a[5]
    C1 = a[6]
    C2 = a[7]
    
    x_1 = X[0:int(X_y)]
    
    #for x > y, x < 0:
    x_2 = X[int(X_y):int(contact)]
    
    #for x > 0
    x_3 = X[int(contact):]
    
    
    
    #solve the flexure for the 3 sections x_1, x_2 and x_3 respectively:
    w_1 = np.exp((x_1-y) / a_1)*(A1*np.cos((x_1-y)/a_1)+A2*np.sin((x_1-y)/a_1))

    w_2 = (np.exp((y-x_2) / a_1)*(B1*np.cos((x_2-y)/a_1)+B2*np.sin((x_2-y)/a_1)) +
           np.exp((x_2-y) / a_1)*(B3*np.cos((x_2-y)/a_1)+B4*np.sin((x_2-y)/a_1)))    
    
    w_3 = np.exp((-x_3) / a_2)*(C1*np.cos((x_3)/a_2)+C2*np.sin((x_3)/a_2))

    
    W_left = np.append(w_1,w_2)
    W_left = np.append(W_left,w_3)
    
    return -W_left


def Central_Load(a_1,a_2,D_1,D_2,V_0, X, contact):
    """this function calculates the flexure due to a force V_0 at y, where y is 
    on the rigidity contrast. the intiger contact must be defined,
    """
    """extra = 400000    
    X = np.linspace( -extra, extra, (2*extra) + 1)
    contact =  extra #the index s.t. X[contact] = 0 """
    
    M = np.zeros((4,4))
    
    #BC1:
    M[0,0] = 1
    M[0,2] = -1
    
    #BC2:
    M[1,0] = 1/a_1
    M[1,1] = 1/a_1
    M[1,2] = 1/a_2
    M[1,3] = -1/a_2
    
    #BC3:
    M[2,1] = 2*D_1/(a_1**2)
    M[2,3] = 2*D_2/(a_2**2)
    
    #BC4:
    M[3,0] = -2*D_1/(a_1**3)
    M[3,1] = 2*D_1/(a_1**3)
    M[3,2] = -2*D_2/(a_2**3)
    M[3,3] = -2*D_2/(a_2**3)
    
    #create RHS of the boundary conditions:
    m = np.zeros(4)
    m[3] = -V_0
    
    #solve the matrix equation Ma = m:
    a = np.linalg.solve(M,m)
    
    A1 = a[0]
    A2 = a[1]
    C1 = a[2]
    C2 = a[3]
    
    x_1 = X[0:int(contact)]
    x_2 = X[int(contact):]
    
    w_1 = np.exp((x_1) / a_1)*(A1*np.cos(x_1/a_1)+A2*np.sin(x_1/a_1))
    
    w_2 = np.exp((-x_2) / a_2)*(C1*np.cos(x_2/a_2)+C2*np.sin(x_2/a_2))
    
    W_centre = np.append(w_1,w_2)
    
    return -W_centre


def Plate_Flexure_MN(a_1,a_2,D_1,D_2,Force, points_of_weight, n_halfWidth):
    """'a_1, D_1 and a_2, D_2 are for left and right of the contrast respectively.
    Force is the load in array form, points of weight are the locations of the load from left, 
    contact is the location of the flexural regidity change from the Left.
    """
    #Make array of y elements, distance from the flexural contrast (left is negative, right is positive):
    Y = -np.subtract(n_halfWidth, points_of_weight)
    
    X = np.arange(- n_halfWidth , n_halfWidth +1 , 1) #+ add 1 on the right side so to include the element extra_R
        
    #for the Right hand load the flexure will be solved by effectively flipping the geometry around the contact,
    #but the arrays must be the same shape so that the point flexures can be summed together. 
    ##create a flipped X array:
    
    X_flip = -np.flip(X,0)
    #X_flip = -np.arange(- extra_R, extra_L, 1)
    
    #create array to add flexures from each point force into:
    Total_flexure = np.zeros(len(X))
    
    for i, y in enumerate(Y):
        """y needs to be the distance from the contact (right is +), V_0 is the size of force,
        X_y is s.t. X[X_y] = the position of the force."""
        V_0 = Force[i]
        X_y = n_halfWidth + y
        
        if V_0 == 0:
            pass
        
        elif y < 0:
            """needs to input V_0 = weight, y = distance from contact(m), X_y = indic of y in X, X = space array
            contact - indic of contact"""
            
            w_y = Left_Load(a_1,a_2,D_1,D_2,V_0, y, X_y, X, n_halfWidth)
            
        elif y == 0:
            w_y = Central_Load(a_1,a_2,D_1,D_2,V_0, X, n_halfWidth)
            
        else:
            """this function calculates the flexure due to a force V_0 at y, where y is to the 
            right (positive sign) of a rigidity contrast. The flexure is the same as a left-hand flexure except
            with thecoordinates fliped around the contact, the y fliped and the parameters fliped (so that the
            boundary conditions are satisfied)."""
            y = -y
            X_y_fliped = (n_halfWidth + y)
            
            
            #w_y = Right_Load(a_1,a_2,D_1,D_2,V_0, y, X_y_fliped, X_flip, contact_fliped)
            w_y = Left_Load(a_2,a_1,D_2,D_1,V_0, y, X_y_fliped, X_flip, n_halfWidth)
            w_y = np.flip(w_y, 0)
            
        
        Total_flexure = Total_flexure + w_y
        
    
    return [X,Total_flexure, Y]